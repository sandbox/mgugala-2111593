<?php 
/**
 * @file
 * Add wiget to the select list field which allows to use jqueryToUiSlider jQuery library
 */

/**
 * Implements hook_field_widget_info().
 */
function select2slider_field_widget_info() {
  return array(
    'select2slider' => array(
      'label' => t('Slider'),
      'field types' => array('list_integer', 'list_float', 'list_text', 'list_boolean'),
      'behaviors' => array(
        'multiple values' => FIELD_BEHAVIOR_CUSTOM,
      ),
    ),
  );
}
/**
 * Implements hook_field_widget_form().
 */
function select2slider_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  // Abstract over the actual field columns, to allow different field types to
  // reuse those widgets.
  $value_key = key($field['columns']);
  
  $type = $instance['widget']['type'];
  $multiple = $field['cardinality'] > 1 || $field['cardinality'] == FIELD_CARDINALITY_UNLIMITED;
  $required = $element['#required'];
  $has_value = isset($items[0][$value_key]);
  $properties = _options_properties($type, $multiple, $required, $has_value);
  
  $entity_type = $element['#entity_type'];
  $entity = $element['#entity'];
  
  // Prepare the list of options.
  $options = _options_get_options($field, $instance, $properties, $entity_type, $entity);
  
  // Put current field values in shape.
  $default_value = _options_storage_to_form($items, $options, $value_key, $properties);
  
  switch($type) {
    case 'select2slider':
      $element += array(
        '#type' => 'select',
        '#default_value' => $default_value,
        '#multiple' => $multiple,
        '#options' => $options,
        '#attributes' => array('class' => array('select2slider')),
        '#attached' => array(
          'library' => array(
            array('system', 'ui.slider'),
          ),
          'libraries_load' => array(
            array('selecttouislider'),
          ),
          'js' => array(drupal_get_path('module', 'select2slider') . '/select2slider.js'),
          'css' => array(drupal_get_path('module', 'select2slider') . '/select2slider.css'),
        ),
      );
      break;
  }
  
  $element += array(
    '#value_key' => $value_key,
    '#element_validate' => array('options_field_widget_validate'),
    '#properties' => $properties,
  );
  
  return $element;
}
/**
 * Implements hook_form_field_ui_field_edit_form_alter().
 */
function select2slider_form_field_ui_field_edit_form_alter(&$form, &$form_state, $form_id) {
  if ($form['#instance']['widget']['type'] == 'select2slider') {
    $form['field']['cardinality']['#options'] = array(1 => 1);
    $form['field']['cardinality']['#description'] = t('Cardinality is disabled by <em>Select to slider</em> module. This module allows only one value.');
  }
}
/**
 * Implements hook_libraries_info().
 */
function select2slider_libraries_info() {
  $libraries['selecttouislider'] = array(
      'name' => 'Select to UI Slider',
      'vendor url' => 'http://filamentgroup.com/lab/update_jquery_ui_slider_from_a_select_element_now_with_aria_support/',
      'download url' => 'http://filamentgroup.com/examples/slider_v2/FilamentGroup_selectToUISlider.zip',
      'version arguments' => array(
        'file' => 'js/selectToUISlider.jQuery.js',
        'pattern' => '/.*\/slider_v([0-9]+)\/.*/',
        'lines' => 15,
      ),
      'files' => array(
        'js' => array(
          'js/selectToUISlider.jQuery.js',
        ),
        'css' => array(
          'css/ui.slider.extras.css' => array(),
          //'css/redmond/jquery-ui-1.7.1.custom.css' => array()
        ),
      ),
  );

  return $libraries;
}