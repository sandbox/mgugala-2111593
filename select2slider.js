(function ($) {
  Drupal.behaviors.select2slider = {
	attach: function(context) {
	  $.each($('select.select2slider'), function() {
		var select = this.id;
		
	    $('select#' + select).selectToUISlider({
	      sliderOptions: {
		    stop: function(e,ui) {
		      $('select#' + select).change();
		    }
		  }
		}).hide().parent().parent().addClass('select2slider');
	    $('.ui-slider-scale li:first').addClass('first');
	  });
	}
  }
}) (jQuery);